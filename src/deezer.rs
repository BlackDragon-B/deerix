use std::time::Duration;

use reqwest::blocking::Client;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};

use crate::config::BaseConfig;
use crate::enums::{DeezerApiRequest, TrackFormats};
use crate::errors::DeerixError::{ConnectionError, TrackNotAvailable};
use crate::errors::{print_iteration_error, DeerixError};
use crate::thread_messages::thread_sleep;

// TODO change Value to model value
pub fn get_internal_user_data(client: &mut Client, api_request: DeezerApiRequest, api_token: Option<String>) -> Result<Value, String> {
    #[derive(Debug, Serialize)]
    struct DeezerApiRequestInternal<'a> {
        api_version: &'a str,
        api_token: &'a str,
        input: &'a str,
        #[serde(flatten)]
        deezer_api_reqeust: DeezerApiRequest,
    }

    let api_token_string = api_token.unwrap_or_else(|| "null".to_string());

    let deezer_api_request = DeezerApiRequestInternal {
        api_version: "1.0",
        api_token: &api_token_string,
        input: "3",
        deezer_api_reqeust: api_request,
    };

    let mut iterations_left = 5; // Quota should not be a problem here
    loop {
        let rep = client
            .post("http://www.deezer.com/ajax/gw-light.php")
            .json(&deezer_api_request)
            .timeout(Duration::from_secs(30))
            .send();
        match rep {
            Ok(rep) => match rep.text() {
                Ok(resp_text) => {
                    let json_value: Value = serde_json::from_str(&resp_text).unwrap();
                    return Ok(json_value);
                }
                Err(e) => {
                    if iterations_left == 0 {
                        return Err(e.to_string());
                    }
                    print_iteration_error(e.to_string());
                    iterations_left -= 1;
                }
            },
            Err(err) => {
                if iterations_left == 0 {
                    return Err(err.to_string());
                }
                print_iteration_error(err.to_string());
                iterations_left -= 1;
            }
        }
    }
}

// TODO Re-enable when will need to use non
// pub fn get_user_id(client: &mut Client) -> Result<i64, anyhow::Error> {
//     let json_result = get_internal_user_data(client, DeezerApiRequest::GetUserData, None)?;
//     get_cached_user_id(&json_result)
// }

// pub fn get_user_api_token(client: &mut Client) -> Result<String, anyhow::Error> {
//     let json_result = get_internal_user_data(client, DeezerApiRequest::GetUserData, None)?;
//     let results = &json_result["results"];
//     Ok(results["checkForm"].as_str().unwrap().to_string())
// }

pub fn get_cached_api_token(json_result: &Value) -> String {
    let results = &json_result["results"];
    results["checkForm"].as_str().unwrap().to_string()
}

pub fn get_cached_user_id(json_result: &Value) -> i64 {
    let results = &json_result["results"];
    results["USER"]["USER_ID"].as_i64().unwrap()
}

pub fn get_cached_license_token(json_result: &Value) -> String {
    let results = &json_result["results"];
    results["USER"]["OPTIONS"]["license_token"].as_str().unwrap().to_string()
}

pub fn deezer_download_song_data_data(client: &mut Client, song_id: u32, api_token: &str) -> Value {
    get_internal_user_data(client, DeezerApiRequest::SongData { id: song_id as u64 }, Some(api_token.to_string())).unwrap()
}

fn deezer_get_url(client: &mut Client, base_config: &BaseConfig, token: &str, track_id: u32, format: TrackFormats) -> Result<UrlAllStruct, DeerixError> {
    let json = json!({"license_token" : base_config.license_token,
             "media": [{
                        "type": "FULL",
                        "formats": [
                            { "cipher": "BF_CBC_STRIPE", "format": format.to_string() }
                        ]
                    }],
             "track_tokens": [token]});

    let mut iterations_left = base_config.retrying_number;
    loop {
        let url = "https://media.deezer.com/v1/get_url";

        let resp = match client.post(url).json(&json).timeout(Duration::from_secs(base_config.timeout_seconds)).send() {
            Ok(rep) => rep,
            Err(err) => {
                if iterations_left == 0 {
                    return Err(ConnectionError(Some(track_id), None, url.to_string(), err.to_string()));
                }
                print_iteration_error(err.to_string());
                iterations_left -= 1;
                thread_sleep(0.1, 6.0);
                continue;
            }
        };

        // let mut text = resp.text().unwrap();
        // let json_value: Value = serde_json::from_str(&text).unwrap();
        // match serde_json::from_value::<UrlAllStruct>(json_value) {
        match resp.json::<UrlAllStruct>() {
            Ok(t) => {
                if (t.errors.is_none() && t.data.is_none()) || (t.data.is_some() && t.errors.is_some()) {
                    eprintln!("Deezer API error - both data/errors are visible/none {t:?}");
                    return Err(TrackNotAvailable(track_id, format!("Deezer API error - both data/errors are visible/none {t:?}")));
                }
                let t_clone = t.clone();
                if t.data.is_some() && t_clone.data.unwrap()[0].errors.is_none() {
                    return Ok(t);
                }
                if iterations_left == 0 {
                    let errors = if t.errors.is_some() {
                        t.errors.unwrap().into_iter().map(|ue| ue.message).collect::<Vec<String>>().join(",,,")
                    } else {
                        t.data
                            .unwrap()
                            .into_iter()
                            .flat_map(|ue| ue.errors.unwrap())
                            .map(|ue| ue.message)
                            .collect::<Vec<String>>()
                            .join(",,,")
                    };
                    return Err(TrackNotAvailable(track_id, errors));
                }
                iterations_left -= 1;
            }
            Err(e) => {
                if iterations_left == 0 {
                    //dbg!(&e);
                    return Err(TrackNotAvailable(track_id, e.to_string()));
                }
                iterations_left -= 1;
            }
        };
    }
}

pub fn deezer_get_track_token(client: &mut Client, api_token: &str, track_id: u32) -> String {
    let data = &deezer_download_song_data_data(client, track_id, api_token);

    // dbg!(&data);
    let data = &data["results"];
    let token = if let Some(fallback) = &data.get("FALLBACK") {
        fallback.get("TRACK_TOKEN")
    } else {
        data.get("TRACK_TOKEN")
    }
    .unwrap()
    .as_str()
    .unwrap()
    .to_string();

    token
}

pub fn deezer_generate_url(client: &mut Client, base_config: &BaseConfig, track_id: u32) -> Result<Vec<(String, &'static str)>, DeerixError> {
    let token = deezer_get_track_token(client, &base_config.api_token, track_id);

    let track_format_data: &[TrackFormats] = if base_config.fallback_quality {
        match base_config.quality {
            TrackFormats::MP3_128 => &[TrackFormats::MP3_128],
            TrackFormats::MP3_320 => &[TrackFormats::MP3_320, TrackFormats::MP3_128],
            TrackFormats::FLAC => &[TrackFormats::FLAC, TrackFormats::MP3_320, TrackFormats::MP3_128],
        }
    } else {
        match base_config.quality {
            TrackFormats::MP3_128 => &[TrackFormats::MP3_128],
            TrackFormats::MP3_320 => &[TrackFormats::MP3_320],
            TrackFormats::FLAC => &[TrackFormats::FLAC],
        }
    };

    let mut collected_urls = Vec::new();
    for track_format in track_format_data {
        let all_struct = deezer_get_url(client, base_config, &token, track_id, *track_format)?;
        let media = all_struct
            .data
            .unwrap()
            .get(0)
            .unwrap_or_else(|| panic!("Please report that data from in json encoding crashes with track {track_id}"))
            .clone()
            .media
            .unwrap();
        let extension = match track_format {
            TrackFormats::FLAC => "flac",
            _ => "mp3",
        };

        let urls = media.into_iter().flat_map(|e| e.sources).map(|e| (e.url, extension)).collect::<Vec<_>>();

        collected_urls.extend(urls);
    }

    if collected_urls.is_empty() {
        return if base_config.fallback_quality || base_config.quality == TrackFormats::MP3_128 {
            Err(TrackNotAvailable(track_id, "Deerix cannot find urls for this quality".to_string()))
        } else {
            //dbg_debug!();
            Err(TrackNotAvailable(
                track_id,
                "Deerix cannot find urls for this quality, consider to use fallback to try download song with lower quality.".to_string(),
            ))
        };
    }

    Ok(collected_urls)
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
struct UrlAllStruct {
    data: Option<Vec<UrlData>>,
    errors: Option<Vec<UrlError>>,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
struct UrlData {
    media: Option<Vec<UrlMedia>>,
    errors: Option<Vec<UrlError>>,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
struct UrlMedia {
    cipher: UrlCipher,
    exp: u32,
    format: String,
    media_type: String,
    nbf: u32,
    sources: Vec<UrlSources>,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
struct UrlCipher {
    r#type: String,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
struct UrlSources {
    provider: String,
    url: String,
}

#[allow(dead_code)]
#[derive(Debug, Deserialize, Clone)]
struct UrlError {
    code: i32,
    message: String,
}
