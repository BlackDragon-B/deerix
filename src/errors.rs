#![macro_use]

use std::fmt;
use std::fmt::Debug;

#[macro_export]
macro_rules! dbg_debug {
    () => {
        // USE TRUE TO DEBUG PRINT in random places in code to fix/find deadlocks
        if false {
            dbg!();
        }
    };
}

/// Contains all possible Deerix errors
#[derive(Debug, Clone)]
pub enum DeerixError {
    ConnectionError(Option<u32>, Option<u32>, String, String),
    AlbumsGetError(u32, String),
    TrackAlbumGetError(u32, String),
    ArtistNotExists(u32, String),
    DecodeError(u32, u32, String),
    FolderSaveError(u32, u32, String, String),
    FileSaveError(u32, u32, String, String),
    MetadataSaveError(u32, u32, String, String),
    ContentTooBig(u32, u32, u64),
    TrackNotAvailable(u32, String),
}

impl fmt::Display for DeerixError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            DeerixError::ArtistNotExists(id, error) => write!(f, "Failed to found artist with ID {id}, error - {error}"),
            DeerixError::ConnectionError(track_id, album_id, url, error) => {
                write!(f, "Failed to get connect to {url} - error - {error}, track_id{track_id:?}, album_id - {album_id:?}")
            }
            DeerixError::AlbumsGetError(artist_id, error) => write!(f, "Failed to get album data from deemix - artist_id - {artist_id}, error - {error}"),
            DeerixError::TrackAlbumGetError(album_id, error) => {
                write!(f, "Failed to get info about tracks in album - album_id - {album_id}, error - {error}")
            }
            DeerixError::DecodeError(track_id, album_id, error) => write!(f, "Failed to get decode file - track_id - {track_id}, album_id - {album_id}, error - {error}"),
            DeerixError::FolderSaveError(track_id, album_id, save_path, error) => {
                write!(
                    f,
                    "Failed to create folder - track_id - {track_id}, album_id - {album_id}, save_path - {save_path} error - {error}"
                )
            }
            DeerixError::FileSaveError(track_id, album_id, save_path, error) => {
                write!(
                    f,
                    "Failed to save file - track_id - {track_id}, album_id - {album_id}, save_path - {save_path} error - {error}"
                )
            }
            DeerixError::MetadataSaveError(track_id, album_id, save_path, error) => {
                write!(
                    f,
                    "Failed to save metadata to file - track_id - {track_id}, album_id - {album_id}, save_path - {save_path} error - {error}"
                )
            }
            DeerixError::ContentTooBig(track_id, album_id, size) => write!(
                f,
                "Failed to download file, maximum allowed size 40MB, this one had {} MB track_id - {track_id}, album_id - {album_id}",
                (size / 1024 / 1024)
            ),
            DeerixError::TrackNotAvailable(track_id, error) => write!(f, "Failed to download track file because is not available - track_id - {track_id}, error - {error}"),
        }
    }
}

pub fn print_iteration_error<T>(_any: T)
where
    T: Debug,
{
    // println!("_____ Iteration error {any:?}");
}
